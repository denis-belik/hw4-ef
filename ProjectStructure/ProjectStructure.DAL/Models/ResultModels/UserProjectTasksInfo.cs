﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DAL.Models.ResultModels
{
	public class UserProjectTasksInfo
	{
		public User User { get; set; }
		public Project LatestProject { get; set; }
		public int LatestProjectTasksCount { get; set; }
		public int UsersNotFinishedTasksCount { get; set; }
		public Task UsersLongestDurationTask { get; set; }
	}
}
