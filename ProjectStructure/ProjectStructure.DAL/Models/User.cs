﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.DAL.Models
{
	public class User
	{
		public int Id { get; set; }

		[Required, StringLength(32), RegularExpression("^([A-Za-z])+$", ErrorMessage = "FirstName can contain only latin letters.")]
		public string FirstName { get; set; }

		[Required, StringLength(32), RegularExpression("^([A-Za-z])+$", ErrorMessage = "LastName can contain only latin letters.")]
		public string LastName { get; set; }

		[Required, StringLength(64), EmailAddress]
		public string Email { get; set; }

		public DateTime Birthday { get; set; }
		public DateTime RegisteredAt { get; set; }

		[Required, StringLength(32)]
		public string City { get; set; }

		public int? TeamId { get; set; }

		public Team Team { get; set; }
		public IEnumerable<Task> Tasks { get; set; }
		public IEnumerable<Project> Projects { get; set; }

		public User() { }
		public User(User user)
		{
			Id = user.Id;
			FirstName = user.FirstName;
			LastName = user.LastName;
			Email = user.Email;
			Birthday = user.Birthday;
			RegisteredAt = user.RegisteredAt;
			TeamId = user.TeamId;
			City = user.City;

			Team = user.Team;
			Tasks = user.Tasks;
			Projects = Projects;
		}

		public User(User user, Team team) : this(user)
		{
			Team = team;
		}

		public User(User user, IEnumerable<Task> tasks) : this(user)
		{
			Tasks = tasks;
		}

		public User(User user, IEnumerable<Project> projects) : this(user)
		{
			Projects = projects;
		}
	}
}
