﻿
namespace ProjectStructure.DAL.Models
{
	public enum TaskState
	{
		Created,
		Started,
		Finished,
		Canceled
	}
}
