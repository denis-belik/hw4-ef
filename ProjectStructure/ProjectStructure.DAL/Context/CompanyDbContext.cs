﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Json;
using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.IO;

namespace ProjectStructure.DAL.Context
{
	public class CompanyDbContext : DbContext
	{
		public CompanyDbContext(DbContextOptions<CompanyDbContext> options)
			: base(options)
		{

		}

		public DbSet<User> Users { get; set; }
		public DbSet<Project> Projects { get; set; }
		public DbSet<Task> Tasks { get; set; }
		public DbSet<Team> Teams { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Team>()
				.Property(team => team.Name)
				.HasColumnName("TeamName");

			string path =
					Directory.GetParent(
						Directory.GetCurrentDirectory()).FullName + @"\ProjectStructure.DAL\Json";

			List<User> userList = Deserializator.DeserializeCollection<User>(path, "users");
			List<Project> projectList = Deserializator.DeserializeCollection<Project>(path, "projects");
			List<Task> taskList = Deserializator.DeserializeCollection<Task>(path, "tasks");
			List<Team> teamList = Deserializator.DeserializeCollection<Team>(path, "teams");

			modelBuilder.Entity<User>().HasData(userList);
			modelBuilder.Entity<Project>().HasData(projectList);
			modelBuilder.Entity<Task>().HasData(taskList);
			modelBuilder.Entity<Team>().HasData(teamList);
		}
	}
}
