﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Commands.Handlers;
using ProjectStructure.BLL.Commands.Teams;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Handlers;
using ProjectStructure.BLL.Queries.Teams;
using ProjectStructure.Common.DTO.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TeamsController : ControllerBase
	{
		TeamsQueryHandler _queryHandler;
		TeamsCommandHandler _commandHandler;

		public TeamsController(TeamsQueryHandler queryHandler, TeamsCommandHandler commandHandler)
		{
			_queryHandler = queryHandler;
			_commandHandler = commandHandler;
		}

		[HttpGet]
		public IActionResult Get()
		{
			try
			{
				var teamsDto = _queryHandler.Handle(new GetAllQuery());
				return Ok(teamsDto);
			}
			catch(Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpGet("MembersByAge")]
		public IActionResult TeamMembersByAge(int minAge = 10)
		{
			try
			{
				var teamMembers = _queryHandler.Handle(new GetTeamMembersByAgeQuery { MinAge = minAge });
				return Ok(teamMembers);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpGet("{id}")]
		public IActionResult Get(int id)
		{
			try
			{
				var teamDto = _queryHandler.Handle(new GetByIdQuery(id));
				return Ok(teamDto);
			}
			catch(NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}	
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] TeamCreateDTO teamDto)
		{
			try
			{
				int createdId = await _commandHandler.Handle(new AddTeamCommand { TeamCreateDto = teamDto });
				return Created($"api/tasks/{createdId}", teamDto);
			}
			catch (ArgumentException ex)
			{
				return BadRequest(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}	
		}

		[HttpPut]
		public async Task<IActionResult> Put([FromBody] TeamUpdateDTO teamUpdateDto)
		{
			try
			{
				await _commandHandler.Handle(new UpdateTeamCommand { TeamUpdateDto = teamUpdateDto });
				return NoContent();
			}
			catch (ArgumentException ex)
			{
				return BadRequest(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			try
			{
				await _commandHandler.Handle(new DeleteTeamCommand { TeamId = id });
				return NoContent();
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}
	}
}
