﻿using System;

namespace ProjectStructure.Common.DTO.Project
{
	public class ProjectCreateDTO
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime CreatedAt { get; set; } = DateTime.Now;
		public DateTime Deadline { get; set; }
		public int AuthorId { get; set; }
		public int TeamId { get; set; }
	}
}
