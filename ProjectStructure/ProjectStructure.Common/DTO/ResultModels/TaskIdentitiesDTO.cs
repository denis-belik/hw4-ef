﻿

namespace ProjectStructure.Common.DTO.ResultModels
{
	public class TaskIdentitiesDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
