﻿using ProjectStructure.Common.DTO.Project;

namespace ProjectStructure.Common.DTO.ResultModels
{
	public class FakeProjectTasksCountDictionaryDTO
	{
		public ProjectDTO Project { get; set; }
		public int TasksCount { get; set; }
	}
}
