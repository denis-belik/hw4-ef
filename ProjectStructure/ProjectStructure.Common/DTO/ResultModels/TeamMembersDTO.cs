﻿using ProjectStructure.Common.DTO.User;
using System.Collections.Generic;

namespace ProjectStructure.Common.DTO.ResultModels
{
	public class TeamMembersDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public IEnumerable<UserDTO> Members { get; set; }
	}
}
