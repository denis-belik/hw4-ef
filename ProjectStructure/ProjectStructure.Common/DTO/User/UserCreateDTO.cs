﻿using System;

namespace ProjectStructure.Common.DTO.User
{
	public class UserCreateDTO
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public DateTime Birthday { get; set; }
		public DateTime RegisteredAt { get; set; } = DateTime.Now;
		public string City { get; set; }
	}
}
