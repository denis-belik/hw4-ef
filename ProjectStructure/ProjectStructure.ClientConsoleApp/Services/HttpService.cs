﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ProjectStructure.ClientConsoleApp
{
	public class HttpService
	{
		public string ApiUrl { get; set; }
		public HttpClient httpClient = new HttpClient();

		public HttpService(string apiUrl)
		{
			ApiUrl = apiUrl;
		}

		public async Task<List<T>> GetCollection<T>(string route, Dictionary<string, string> queryParams = null)
		{
			var builder = new UriBuilder(ApiUrl + route);

			if (queryParams != null)
			{
				var query = HttpUtility.ParseQueryString(builder.Query);
				foreach (var param in queryParams)
				{
					query[param.Key] = param.Value;
				}
				builder.Query = query.ToString();
			}

			string url = builder.ToString();

			var response = await httpClient.GetAsync(url);
			response.EnsureSuccessStatusCode();

			string contentString = await response.Content.ReadAsStringAsync();

			var entities = JsonConvert.DeserializeObject<List<T>>(contentString);

			return entities;
		}

		public async Task<T> GetEntity<T>(string route, Dictionary<string,string> queryParams = null)
		{

			var builder = new UriBuilder(ApiUrl + route);

			if(queryParams != null)
			{
				var query = HttpUtility.ParseQueryString(builder.Query);
				foreach (var param in queryParams)
				{
					query[param.Key] = param.Value;
				}
				builder.Query = query.ToString();
			}

			string url = builder.ToString();

			var response = await httpClient.GetAsync(url);	
			string contentString = await response.Content.ReadAsStringAsync();

			if (!response.IsSuccessStatusCode)
			{
				throw new Exception(contentString);
			}

			var entity = JsonConvert.DeserializeObject<T>(contentString);
			return entity;
		}

		public async Task<string> PostEntity<T>(string route, T entity)
		{
			string json = JsonConvert.SerializeObject(entity);
			var data = new StringContent(json, Encoding.UTF8, "application/json");
			var response = await httpClient.PostAsync(ApiUrl + route, data);

			string responseMessage = await response.Content.ReadAsStringAsync();
			if (!response.IsSuccessStatusCode)
			{
				throw new Exception(responseMessage);
			}

			return responseMessage;
		}

		public async Task<bool> PutEntity<T>(string route, T entity)
		{
			string json = JsonConvert.SerializeObject(entity);
			var data = new StringContent(json, Encoding.UTF8, "application/json");
			var response = await httpClient.PutAsync(ApiUrl + route, data);
			string responseMessage = await response.Content.ReadAsStringAsync();
			if (!response.IsSuccessStatusCode)
			{
				throw new Exception(responseMessage);
			}
			return response.IsSuccessStatusCode;
		}

		public async Task<bool> DeleteEntity(string route, int entityId)
		{
			var response = await httpClient.DeleteAsync($"{ApiUrl}{route}/{entityId}");
			string responseMessage = await response.Content.ReadAsStringAsync();
			if (!response.IsSuccessStatusCode)
			{
				throw new Exception(responseMessage);
			}
			return response.IsSuccessStatusCode;
		}
	}
}
