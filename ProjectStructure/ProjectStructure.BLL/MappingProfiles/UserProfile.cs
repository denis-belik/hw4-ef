﻿using AutoMapper;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.MappingProfiles
{
	public sealed class UserProfile : Profile
	{
		public UserProfile()
		{
			CreateMap<UserDTO, User>();
			CreateMap<User, UserDTO>();

			CreateMap<UserCreateDTO, User>();

			CreateMap<User, UserWithTasksDTO>();
		}
	}
}
