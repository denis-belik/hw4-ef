﻿

namespace ProjectStructure.BLL.Queries.Teams
{
	public class GetTeamMembersByAgeQuery
	{
		public int MinAge { get; set; } = 10;
	}
}
