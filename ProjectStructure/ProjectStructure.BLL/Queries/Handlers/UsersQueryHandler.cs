﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Abstractions.Queries.Handlers;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Users;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.Common.Helpers;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.Models.ResultModels;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Queries.Handlers
{
	public class UsersQueryHandler : QueryHandler<UserDTO>
	{
		public UsersQueryHandler(CompanyDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }
		
		public override UserDTO Handle(GetByIdQuery query)
		{
			var user = _context.Users.FirstOrDefault(user => user.Id == query.EntityId);

			if (user == null)
			{
				throw new NotFoundException(nameof(User), query.EntityId); 
			}

			return _mapper.Map<UserDTO>(user);
		}

		public override IEnumerable<UserDTO> Handle(GetAllQuery _)
		{
			var users = _context.Users;
			return _mapper.Map<ICollection<UserDTO>>(users);
		}

		// 1. Отримати кількість тасків у проекті конкретного користувача(по id) (словник, де key буде проект, а value кількість тасків).
		public Dictionary<ProjectDTO, int> Handle(GetProjectTasksCountQuery query)
		{
			if (!_context.Users.Any(user => user.Id == query.ProjectAuthorId))
			{
				throw new NotFoundException(nameof(User), query.ProjectAuthorId);
			}

			return _context.Projects
				.Include(project => project.Tasks)
				.Where(project => project.AuthorId == query.ProjectAuthorId)
				.ToDictionary(project =>
					_mapper.Map<ProjectDTO>(project),
					project => project.Tasks.Count());
		}

		// 5. Отримати список користувачів за алфавітом first_name(по зростанню) з відсортованими tasks по довжині name(за спаданням).
		public IEnumerable<UserWithTasksDTO> Handle(GetSortedUsersWithTasksQuery _)
		{
			var orderedTasks = _context.Tasks.OrderByDescending(task => task.Name.Length).ToList();

			var users =
				_context.Users
				.Include(user => orderedTasks)
				.OrderBy(user => user.FirstName)
				.ToList();

			return _mapper.Map<ICollection<UserWithTasksDTO>>(users);
		}

		// 6. Отримати наступну структуру (передати Id користувача в параметри):
		// User
		// Останній проект користувача (за датою створення)
		// Загальна кількість тасків під останнім проектом
		// Загальна кількість незавершених або скасованих тасків для користувача
		// Найтриваліший таск користувача за датою (найраніше створений - найпізніше закінчений)
		public UserProjectTasksInfoDTO Handle(GetUserProjectTasksInfoQuery query)
		{
			if(!_context.Users.Any(user => user.Id == query.UserId))
			{
				throw new NotFoundException(nameof(User), query.UserId);
			}

			var orderedProjects = _context.Projects
				.Include(project => project.Tasks)
				.OrderByDescending(project => project.CreatedAt).ToList();

			var userProjectTasksInfo = _context.Users
				.Include(user => orderedProjects)
				.Include(user => user.Tasks)
				.Where(user => user.Id == query.UserId)
				.ToList()
				.Select(user => new UserProjectTasksInfo
				{
					User = user,

					LatestProject = user.Projects?.FirstOrDefault(),

					LatestProjectTasksCount = user.Projects == null || user.Projects.FirstOrDefault().Tasks == null 
											? 0
											: user.Projects.FirstOrDefault().Tasks.Count(),

					UsersNotFinishedTasksCount = user.Tasks != null 
											   ? user.Tasks.Count(task => task.State != TaskState.Finished) 
											   : 0,

					UsersLongestDurationTask = user.Tasks?
						.OrderByDescending(task => task.FinishedAt - task.CreatedAt)
						.FirstOrDefault()
				})
				.FirstOrDefault();

			return _mapper.Map<UserProjectTasksInfoDTO>(userProjectTasksInfo);
		}
	}
}
