﻿

namespace ProjectStructure.BLL.Queries.Tasks
{
	public class GetTasksByNameLengthQuery
	{
		public int PerformerId { get; set; }
		public int MaxTaskNameLegth { get; set; } = 45;
	}
}
