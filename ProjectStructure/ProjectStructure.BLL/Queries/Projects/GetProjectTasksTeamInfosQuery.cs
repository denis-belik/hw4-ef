﻿

namespace ProjectStructure.BLL.Queries.Projects
{
	public class GetProjectTasksTeamInfosQuery
	{
		public int MinDescriptionLength { get; set; } = 20;
		public int MaxTaskCount { get; set; } = 3;
	}
}
