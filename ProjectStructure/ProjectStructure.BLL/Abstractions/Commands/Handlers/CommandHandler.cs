﻿using AutoMapper;
using ProjectStructure.BLL.Commands.Users;
using ProjectStructure.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Abstractions.Commands.Handlers
{
	public abstract class CommandHandler
	{
		protected readonly CompanyDbContext _context;
		protected readonly IMapper _mapper;

		public CommandHandler(CompanyDbContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		public abstract Task<int> Handle(AddCommand command);
		public abstract Task Handle(DeleteCommand command);
		public abstract Task Handle(UpdateCommand command);
	}
}
