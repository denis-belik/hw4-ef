﻿

namespace ProjectStructure.BLL.Abstractions.Queries
{
	public class GetByIdQuery
	{
		public int EntityId { get; set; }

		public GetByIdQuery(int id)
		{
			EntityId = id;
		}
	}
}
