﻿using AutoMapper;
using ProjectStructure.DAL.Context;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Abstractions.Queries.Handlers
{
	public abstract class QueryHandler<T>
	{
		protected readonly CompanyDbContext _context;
		protected readonly IMapper _mapper;

		public QueryHandler(CompanyDbContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		public abstract T Handle(GetByIdQuery query);
		public abstract IEnumerable<T> Handle(GetAllQuery query);
	}
}
