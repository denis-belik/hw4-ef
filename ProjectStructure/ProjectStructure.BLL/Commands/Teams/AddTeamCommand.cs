﻿using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.Common.DTO.Team;

namespace ProjectStructure.BLL.Commands.Teams
{
	public class AddTeamCommand : AddCommand
	{
		public TeamCreateDTO TeamCreateDto { get; set; }
	}
}
