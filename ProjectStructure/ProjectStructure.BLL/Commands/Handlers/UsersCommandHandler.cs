﻿using ProjectStructure.BLL.Commands.Users;
using ProjectStructure.DAL.Context;
using System.Linq;
using AutoMapper;
using ProjectStructure.DAL.Models;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Abstractions.Commands.Handlers;
using ProjectStructure.BLL.Abstractions.Commands;
using Async = System.Threading.Tasks;
using ProjectStructure.Common.Helpers;

namespace ProjectStructure.BLL.Commands.Handlers
{
	public class UsersCommandHandler : CommandHandler
	{
		public UsersCommandHandler(CompanyDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }
			
		public override async Async.Task<int> Handle(AddCommand addCommand)
		{
			AddUserCommand command = addCommand as AddUserCommand;

			User user = _mapper.Map<User>(command.UserCreateDto);

			// validate properties due to data annotations
			ValidationHelper.ValidateProperties<User>(user);

			_context.Users.Add(user);
			await _context.SaveChangesAsync();

			return user.Id;
		}

		public override async Async.Task Handle(DeleteCommand deleteCommand)
		{
			DeleteUserCommand command = deleteCommand as DeleteUserCommand;

			User user = _context.Users.FirstOrDefault(user => user.Id == command.UserId);

			if(user == null)
			{
				throw new NotFoundException(nameof(User), command.UserId);
			}

			// remove connected entities
			_context.Tasks.RemoveRange(
				_context.Tasks.Where(task => task.PerformerId == user.Id));

			_context.Users.Remove(user);
			await _context.SaveChangesAsync();
		}

		public override async Async.Task Handle(UpdateCommand updateCommand)
		{
			UpdateUserCommand command = updateCommand as UpdateUserCommand;
			var userUpdate = command.UserUpdateDto;

			User userEntity = _context.Users.FirstOrDefault(user => user.Id == userUpdate.Id);

			if (userEntity == null)
			{
				throw new NotFoundException(nameof(User), command.UserUpdateDto.Id);
			}
			
			userEntity.FirstName = string.IsNullOrEmpty(userUpdate.FirstName?.Trim()) 
								 ? userEntity.FirstName
								 : userUpdate.FirstName;

			userEntity.LastName = string.IsNullOrEmpty(userUpdate.LastName?.Trim()) 
								? userEntity.LastName
								: userUpdate.LastName;

			userEntity.Email = string.IsNullOrEmpty(userUpdate.Email?.Trim()) 
							 ? userEntity.Email
							 : userUpdate.Email;

			userEntity.City = string.IsNullOrEmpty(userUpdate.City?.Trim())
							 ? userEntity.City
							 : userUpdate.City;

			if (userUpdate.TeamId == null) // kick user from team
			{
				userEntity.TeamId = userUpdate.TeamId;
			}
			else
			{
				if(command.UserUpdateDto.TeamId > -1)
				{
					if (!_context.Teams.Any(team => team.Id == command.UserUpdateDto.TeamId))
					{
						throw new NotFoundException(nameof(Team), (int)command.UserUpdateDto.TeamId);
					}
					userEntity.TeamId = command.UserUpdateDto.TeamId;
				}
			}

			// validate properties due to data annotations
			ValidationHelper.ValidateProperties<User>(userEntity);

			_context.Update(userEntity);
			await _context.SaveChangesAsync();
		}
	}
}
