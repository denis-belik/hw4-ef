﻿using ProjectStructure.BLL.Abstractions.Commands;

namespace ProjectStructure.BLL.Commands.Users
{
	public class DeleteUserCommand : DeleteCommand
	{
		public int UserId { get; set; }
	}
}
