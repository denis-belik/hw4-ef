﻿using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.BLL.Commands.Tasks
{
	public class UpdateTaskCommand : UpdateCommand
	{
		public TaskUpdateDTO TaskUpdateDto { get; set; }
	}
}
