﻿using ProjectStructure.BLL.Abstractions.Commands;

namespace ProjectStructure.BLL.Commands.Tasks
{
	public class DeleteTaskCommand : DeleteCommand
	{
		public int TaskId { get; set; }
	}
}
